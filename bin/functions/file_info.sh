#!/bin/bash

function file_info_count {
	PATH_FILE_PATTERN=$1
	# Formato de ruta del archivo: /staging/sftpaltamira/DWH_ARG_AA_TRAFICO_LLAMADAS_YYYYMMDDHHMMSS[_F].dat.[Pospago].gz
	COUNT=$(hdfs dfs -ls -h $PATH_FILE_PATTERN | awk '{print $8}' | sort -r | awk NF | wc -l)
	
	echo $COUNT
}

function file_info_get_name {
	PATH_FILE=$1
	# Formato de ruta del archivo: /staging/sftpaltamira/DWH_ARG_AA_TRAFICO_LLAMADAS_YYYYMMDDHHMMSS[_F].dat.[Pospago].gz
	FILE_NAME=$(echo $PATH_FILE | rev | cut -d'/' -f 1 | rev)
	
	echo $FILE_NAME
}

function file_info_get_upload_date {
	FILE_NAME=$1
	# Formato de archivo: DWH_ARG_AA_TRAFICO_LLAMADAS_YYYYMMDDHHMMSS[_F].dat.[Pospago].gz
	LAST_UPLOAD=$(echo $FILE_NAME | cut -d'.' -f 1 | cut -d'_' -f 6 | awk '{print substr($1,1,8)}')
	
	echo $LAST_UPLOAD
}
