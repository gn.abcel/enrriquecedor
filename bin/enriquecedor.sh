set -x
### Seteo nombre de proceso
export PROCESS_NAME="enriquecedor"

### Paths utilizados
export CURRENT_DIR=$(dirname ${BASH_SOURCE[0]})
export APP_DIR=$CURRENT_DIR/..
export PATH_BIN=$APP_DIR/bin
export PATH_FUNCTIONS=$PATH_BIN/functions
export PATH_BIN_CFG=$PATH_BIN/bin_cfg
export PATH_CONF=$APP_DIR/conf
export PATH_LOG=$APP_DIR/log
export PATH_SQL=$APP_DIR/SQL
export PATH_TMP=$APP_DIR/tmp

### Imports de funciones
. $PATH_FUNCTIONS/parse_yaml.sh
. $PATH_FUNCTIONS/send_mail.sh
. $PATH_FUNCTIONS/log.sh
. $PATH_FUNCTIONS/file_info.sh
. $PATH_FUNCTIONS/hive_info.sh

### Leyendo Archivo de Configuracion del proceso
PARAMETROS=$PATH_CONF"/"$PROCESS_NAME"_parameter.yml"
parse_yaml $PARAMETROS > $APP_DIR"/"$PROCESS_NAME"_par"
. $APP_DIR"/"$PROCESS_NAME"_par"

### Obtengo el modo de ejecucuón
AVAILABLE_MODES=$modes
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
	export MODE=$1
else
	echo "Debe ingresar uno de los siguientes modos de ejecucion "$AVAILABLE_MODES
	exit 1
fi

### Leyendo Archivo de Configuracion del modo
PARAMETROS=$PATH_CONF"/mode_"$MODE"_parameter.yml"
parse_yaml $PARAMETROS > $APP_DIR"/"$PROCESS_NAME"_"$MODE"_par"
. $APP_DIR"/"$PROCESS_NAME"_"$MODE"_par"

### Variables internas de fecha
TIMESTAMP_LOG=$(date "+%Y%m%d%H%M%S")
FILE_DATE_N=$2
if [ "$FILE_DATE_N" == "" ]; then
  FILE_DATE_N=$(date --date '-1 day' +%Y%m%d)
fi
CURRENT_DATE_N=$FILE_DATE_N
CURR_DATE=$(date --date="${FILE_DATE_N}" +%Y-%m-%d)
PROCESS_DATE=$(date "+%Y-%m-%d")


### Asuntos de mail
ERROR_MSG=$environment" - ERROR - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE
WARN_MSG=$environment" - WARN - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE
FINISH_MSG=$environment" - OK - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE

### Archivos de log
export PROCESS_LOG_FILE="log_"$PROCESS_NAME"_"$MODE"_"$TIMESTAMP_LOG".log"
export SCREEN_LOG_FILE="logAll_"$PROCESS_NAME"_"$MODE"_"$TIMESTAMP_LOG".log"
export TABLE_LOG_FILE_PREFIX="logTable_"$PROCESS_NAME"_"$MODE"_"
export TABLE_LOG_FILE_SUFIX="_"$TIMESTAMP_LOG".log"

### Log que captura toda la salida del proceso
exec > $PATH_LOG/$SCREEN_LOG_FILE 2>&1

### Hive: conexiones
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_hive.conf"

### Archivos Staging
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_file.conf"

### Carga de tablas ORC

i=1
	while [ "$i" -le "$quantity_steps" ]; do
	
	eval STEP_TABLE='$'tables_step$i

	# Se ejecuta el script de insert a ORC
	$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS -f $PATH_SQL/$STEP_TABLE.sql
		if ! [ $? -eq 0 ]; then
			log 2 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $STEP_TABLE: Se produjo un error durante el paso N°$i del modo $MODE."
		# Notifico el error para salir del ciclo
			PROCESS_ERROR=1
			break
		else
			log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $STEP_TABLE: Este paso se realizo correctamente."
			hdfs dfs -mv $stg_table_path/* $file_path_procesados
		fi
	  i=$(($i + 1))
	done

if ! [ $PROCESS_ERROR -eq 0 ]; then
	# Se envia el log generado por la corrida al usuario
	send_mail "TEXT" "$PATH_LOG/$PROCESS_LOG_FILE" "$ERROR_MSG" "$PATH_LOG/$SCREEN_LOG_FILE"
	exit 1
fi

### Fin del proceso
log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Fin del proceso $PROCESS_NAME $MODE."

# Se envia el log generado por la corrida al usuario
send_mail "TEXT" "$PATH_LOG/$PROCESS_LOG_FILE" "$FINISH_MSG" "$PATH_LOG/$SCREEN_LOG_FILE"

### Se eliminan las configuraciones aplicadas durante la ejecucion
rm -f $APP_DIR"/"$PROCESS_NAME"_"$MODE"_par"
rm -f $APP_DIR"/"$PROCESS_NAME"_par"
set +x
exit 0
