
### Seteo nombre de proceso
export PROCESS_NAME="dimensional"

set -x
### Paths utilizados
export CURRENT_DIR=$(dirname ${BASH_SOURCE[0]})
export APP_DIR=$CURRENT_DIR/..
export PATH_BIN=$APP_DIR/bin
export PATH_PYTHON=$PATH_BIN/python
export PATH_FUNCTIONS=$PATH_BIN/functions
export PATH_BIN_CFG=$PATH_BIN/bin_cfg
export PATH_CONF=$APP_DIR/conf
export PATH_LOG=$APP_DIR/log
export PATH_SQL=$APP_DIR/SQL
export PATH_TMP=$APP_DIR/tmp

### Imports de funciones
. $PATH_FUNCTIONS/parse_yaml.sh
. $PATH_FUNCTIONS/send_mail.sh
. $PATH_FUNCTIONS/log.sh
. $PATH_FUNCTIONS/file_info.sh
. $PATH_FUNCTIONS/hive_info.sh

### Leyendo Archivo de Configuracion del proceso
PARAMETROS=$PATH_CONF"/"$PROCESS_NAME"_parameter.yml"
parse_yaml $PARAMETROS > $APP_DIR"/"$PROCESS_NAME"_par"
. $APP_DIR"/"$PROCESS_NAME"_par"

### Obtengo el modo de ejecucuón
AVAILABLE_MODES=$modes
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
	export MODE=$1
else
	echo "Debe ingresar uno de los siguientes modos de ejecucion "$AVAILABLE_MODES
	exit 1
fi

### Obtengo el submodo de ejecucuón
AVAILABLE_SUBMODES=$submodes
if [[ ",$AVAILABLE_SUBMODES," = *",$2,"* ]]; then
	export SUBMODE=$2
else
	echo "Debe ingresar uno de los siguientes submodos de ejecucion "$AVAILABLE_SUBMODES
	exit 1
fi

### Leyendo Archivo de Configuracion del modo
PARAMETROS=$PATH_CONF"/mode_"$MODE"_parameter.yml"
parse_yaml $PARAMETROS > $APP_DIR"/"$PROCESS_NAME"_"$MODE"_par"
. $APP_DIR"/"$PROCESS_NAME"_"$MODE"_par"

### Variables internas de fecha
TIMESTAMP_LOG=$(date "+%Y%m%d%H%M%S")
FILE_DATE_N=$3
if [ "$FILE_DATE_N" == "" ]; then
  FILE_DATE_N=$(date --date '-1 day' +%Y%m%d)
fi
CURRENT_DATE_N=$FILE_DATE_N
CURR_DATE=$(date --date="${FILE_DATE_N}" +%Y-%m-%d)
PROCESS_DATE=$(date "+%Y-%m-%d")
ONLY_DATE=$(date "+%d")


### Asuntos de mail
ERROR_MSG=$environment" - ERROR - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE
WARN_MSG=$environment" - WARN - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE
FINISH_MSG=$environment" - OK - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE

### Archivos de log
export PROCESS_LOG_FILE="log_"$PROCESS_NAME"_"$MODE"_"$TIMESTAMP_LOG".log"
export SCREEN_LOG_FILE="logAll_"$PROCESS_NAME"_"$MODE"_"$TIMESTAMP_LOG".log"
export TABLE_LOG_FILE_PREFIX="logTable_"$PROCESS_NAME"_"$MODE"_"
export TABLE_LOG_FILE_SUFIX="_"$TIMESTAMP_LOG".log"

### Log que captura toda la salida del proceso
exec > $PATH_LOG/$SCREEN_LOG_FILE 2>&1

### Hive: conexiones
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_hive.conf"

### Archivos Staging
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_file.conf"

### Se genera la query con Python
cd $python_path
eval python generate_sql.py -p $tables_process_name -d $python_path_metadata -f $file_name_input -o proto_$MODE.sql
PYTHON_STATUS=$?
cd -

if ! [ $PYTHON_STATUS -eq 0 ]; then
	log 2 "$PATH_LOG/$PROCESS_LOG_FILE" "Python: Se produjo un error en la generacion del archivo SQL en el modo $MODE en el submodo $SUBMODE."
else
	log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Python: Se genero correctamente el archivo SQL en el modo $MODE en el submodo $SUBMODE."
	mv $python_path_metadata/proto_$MODE.sql $python_path_metadata/$file_name_output
fi

### Carga de tablas ORC
i=0
	while [ "$i" -le "$quantity_steps" ]; do
	
	i=$(($i + 1))	
		
		eval STEP_TABLE='$'tables_step$i
	
		# Verifica si se debe ejecutar el cierre del dia y si es el paso correspondiente
		if [ $i -gt $quantity_steps ];  then 
			if [ $SUBMODE == 'cierre' ] ;  then  
				STEP_TABLE=$tables_step_cierre
			else 
				break
			fi	
		fi
		
		# Se ejecuta el script de insert a ORC
	$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS -f $PATH_SQL/$STEP_TABLE.sql
		if ! [ $? -eq 0 ]; then
			log 2 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $STEP_TABLE: Se produjo un error durante el paso N°$i del modo $MODE en el submodo $SUBMODE."
		# Notifico el error para salir del ciclo
			PROCESS_ERROR=1
			break
		else
			log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $STEP_TABLE: El paso N°$i  correspondiente a la tabla $STEP_TABLE se realizo correctamente."
			PROCESS_ERROR=0
		fi
				
	done

if ! [ $PROCESS_ERROR -eq 0 ]; then
	# Se envia el log generado por la corrida al usuario
	send_mail "TEXT" "$PATH_LOG/$PROCESS_LOG_FILE" "$ERROR_MSG" "$PATH_LOG/$SCREEN_LOG_FILE"
	exit 1
fi

### Fin del proceso
log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Fin del proceso $PROCESS_NAME $MODE $SUBMODE."

# Se envia el log generado por la corrida al usuario
send_mail "TEXT" "$PATH_LOG/$PROCESS_LOG_FILE" "$FINISH_MSG" "$PATH_LOG/$SCREEN_LOG_FILE"

### Se eliminan las configuraciones aplicadas durante la ejecucion
rm -f $APP_DIR"/"$PROCESS_NAME"_"$MODE"_par"
rm -f $APP_DIR"/"$PROCESS_NAME"_par"

set +x
exit 0
