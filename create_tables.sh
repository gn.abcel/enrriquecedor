
CURRENT_DIR=$(dirname ${BASH_SOURCE[0]})
APP_DIR=$CURRENT_DIR
PATH_LOG=$APP_DIR/log
PATH_CREATE=$APP_DIR/CREATES
hive_connection_string='jdbc:hive2://aphdpmngtp10.tmoviles.com.ar:2181,aphdpmngtp11.tmoviles.com.ar:2181,aphdpmngtp09.tmoviles.com.ar:2181,aphdpmngtp07.tmoviles.com.ar:2181,aphdpmngtp08.tmoviles.com.ar:2181/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2;principal=hive/_HOST@TASA.COM'
SET_HIVE_CMD="beeline"
SET_HIVE_STR_CON='-u "'${hive_connection_string}'"'
SET_HIVE_VARS=" --hivevar DBNAME_EDW=edw_prod"
tablas=$(cat $APP_DIR/tablas.txt)
for i in $tablas
do
    ORC_TABLE_NAME=$i
    TABLE_LOG_FILE="LOG_"$ORC_TABLE_NAME".log"
    eval "$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS -f $PATH_CREATE/$ORC_TABLE_NAME.sql" >> $PATH_LOG/$TABLE_LOG_FILE 2>&1 
done
