drop table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp1;
create table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp1 as
select
t0.*,
case when t1.sistema_origen_id is not null then t1.sistema_origen_id else "-3" end sistema_origen_id
from
${DBNAME_EDW}.f_tr_actuacion_detallada_prev t0
left join ${DBNAME_EDW}.d_sistema_origen t1 on t1.sistema_origen_cd ='ALT';

drop table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp2;
create table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp2 as
select
t0.*,
case when (t3.actuacion_subtipo_id ) is not null then (t3.actuacion_subtipo_id ) else "-3" end actuacion_subtipo_id
from
${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp1 t0
left join ${DBNAME_EDW}.d_actuacion_tipo t2 on t0.operacion_actuacion_cd = t2.actuacion_tipo_cd
left join ${DBNAME_EDW}.d_actuacion_subtipo t3 on t0.subactuacion_cd = t3.actuacion_subtipo_cd and t2.actuacion_tipo_id = t3.actuacion_tipo_id;

drop table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp3;
create table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp3 as
select
t0.*,
case when (case when t0.origen_recarga_cd = 'NI' then '-2' else case when t4.metodo_recarga_cd is null then -3 else t4.metodo_recarga_id end end) is not null then (case when t0.origen_recarga_cd = 'NI' then '-2' else case when t4.metodo_recarga_cd is null then -3 else t4.metodo_recarga_id end end) else "-3" end metodo_recarga_id
from
${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp2 t0
left join ${DBNAME_EDW}.d_metodo_recarga t4 on t0.origen_recarga_cd = t4.metodo_recarga_cd;

drop table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp4;
create table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp4 as
select
t0.*,
case when (case when t0.causa_cd = 'NI' then '-2' else case when t5.motivo_ajuste_cd is null then -3 else t5.motivo_ajuste_id end end) is not null then (case when t0.causa_cd = 'NI' then '-2' else case when t5.motivo_ajuste_cd is null then -3 else t5.motivo_ajuste_id end end) else "-3" end motivo_ajuste_id 
from
${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp3 t0
left join ${DBNAME_EDW}.d_motivo_ajuste t5 on t0.causa_cd = t5.motivo_ajuste_cd;

drop table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp5;
create table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp5 as
select
t0.*,
case when t6.moneda_id is not null then t6.moneda_id else "-3" end moneda_id
from
${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp4 t0
left join ${DBNAME_EDW}.d_moneda t6 on t6.moneda_cd ='ARS';

drop table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp6;
create table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp6 as
select
t0.*,
case when (case when t0.bono_cd = 'NI' then '-2' else case when t7.bono_cd is null then -3 else t7.bono_id end end) is not null then (case when t0.bono_cd = 'NI' then '-2' else case when t7.bono_cd is null then -3 else t7.bono_id end end) else "-3" end bono_id
from
${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp5 t0
left join ${DBNAME_EDW}.d_bono t7 on t0.bono_cd = t7.bono_cd;

drop table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp7;
create table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp7 as
select
t0.*,
md5(concat(t0.ani_cd, t0.operacion_fh)) hash_block_7,
case when (case when t0.promocion_cd = 'NI' then '-2' else case when t8.promocion_cd is null then -3 else t8.promocion_id end end ) is not null then (case when t0.promocion_cd = 'NI' then '-2' else case when t8.promocion_cd is null then -3 else t8.promocion_id end end ) else "-3" end promocion_id
from
${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp6 t0
left join ${DBNAME_EDW}.d_promocion t8 on t0.promocion_cd = t8.promocion_cd;

drop table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_final;
create table ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_final as
select
t0.*,
case when t9.suscripcion_id is not null then t9.suscripcion_id else "-3" end suscripcion_id,
case when t9.suscripcion_cd is not null then t9.suscripcion_cd else "-3" end suscripcion_cd,
case when t9.interurbano_cd is not null then t9.interurbano_cd else "-3" end interurbano_cd,
case when t9.urbano_cd is not null then t9.urbano_cd else "-3" end urbano_cd,
case when t9.linea_cd is not null then t9.linea_cd else "-3" end linea_cd,
case when t9.tipo_oferta_id is not null then t9.tipo_oferta_id else "-3" end tipo_oferta_id,
case when t9.tipo_oferta_de is not null then t9.tipo_oferta_de else "Descripcion no disponible" end tipo_oferta_de,
case when t9.catalogo_producto_id is not null then t9.catalogo_producto_id else "-3" end catalogo_producto_id,
case when t9.catalogo_producto_cd is not null then t9.catalogo_producto_cd else "-3" end catalogo_producto_cd,
case when t9.catalogo_producto_de is not null then t9.catalogo_producto_de else "Descripcion no disponible" end catalogo_producto_de,
case when t9.geografia_cnc_id is not null then t9.geografia_cnc_id else "-3" end geografia_cnc_id,
case when t9.geografia_cnc_cd is not null then t9.geografia_cnc_cd else "-3" end geografia_cnc_cd,
case when t9.sub_alm_cd is not null then t9.sub_alm_cd else "-3" end sub_alm_cd,
case when t9.alm_cd is not null then t9.alm_cd else "-3" end alm_cd,
case when t9.sub_region_cnc_id is not null then t9.sub_region_cnc_id else "-3" end sub_region_cnc_id,
case when t9.sub_region_cnc_de is not null then t9.sub_region_cnc_de else "Descripcion no disponible" end sub_region_cnc_de,
case when t9.region_cnc_id is not null then t9.region_cnc_id else "-3" end region_cnc_id,
case when t9.region_cnc_de is not null then t9.region_cnc_de else "Descripcion no disponible" end region_cnc_de,
case when t9.flag_prepago_express_id is not null then t9.flag_prepago_express_id else "-3" end flag_prepago_express_id,
case when t9.operadora_propietaria_id is not null then t9.operadora_propietaria_id else "-3" end operadora_propietaria_id,
case when t9.operadora_propietaria_cd is not null then t9.operadora_propietaria_cd else "-3" end operadora_propietaria_cd,
case when t9.operadora_propietaria_de is not null then t9.operadora_propietaria_de else "Descripcion no disponible" end operadora_propietaria_de,
case when t9.operadora_receptora_id is not null then t9.operadora_receptora_id else "-3" end operadora_receptora_id,
case when t9.operadora_receptora_cd is not null then t9.operadora_receptora_cd else "-3" end operadora_receptora_cd,
case when t9.operadora_receptora_de is not null then t9.operadora_receptora_de else "Descripcion no disponible" end operadora_receptora_de,
case when t9.operadora_donante_id is not null then t9.operadora_donante_id else "-3" end operadora_donante_id,
case when t9.operadora_donante_cd is not null then t9.operadora_donante_cd else "-3" end operadora_donante_cd,
case when t9.operadora_donante_de is not null then t9.operadora_donante_de else "Descripcion no disponible" end operadora_donante_de,
case when t9.flag_volte_id is not null then t9.flag_volte_id else "-3" end flag_volte_id,
case when t9.flag_volte_cd is not null then t9.flag_volte_cd else "-3" end flag_volte_cd,
case when t9.flag_volte_de is not null then t9.flag_volte_de else "Descripcion no disponible" end flag_volte_de,
case when t9.equipo_tecnologia_id is not null then t9.equipo_tecnologia_id else "-3" end equipo_tecnologia_id,
case when t9.equipo_tecnologia_cd is not null then t9.equipo_tecnologia_cd else "-3" end equipo_tecnologia_cd,
case when t9.equipo_tecnologia_de is not null then t9.equipo_tecnologia_de else "Descripcion no disponible" end equipo_tecnologia_de,
case when t9.equipo_utilizado_tecnologia_id is not null then t9.equipo_utilizado_tecnologia_id else "-3" end equipo_utilizado_tecnologia_id,
case when t9.equipo_utilizado_tecnologia_cd is not null then t9.equipo_utilizado_tecnologia_cd else "-3" end equipo_utilizado_tecnologia_cd,
case when t9.equipo_utilizado_tecnologia_de is not null then t9.equipo_utilizado_tecnologia_de else "Descripcion no disponible" end equipo_utilizado_tecnologia_de,
case when t9.catalogo_equipo_id is not null then t9.catalogo_equipo_id else "-3" end catalogo_equipo_id,
case when t9.catalogo_equipo_cd is not null then t9.catalogo_equipo_cd else "-3" end catalogo_equipo_cd,
case when t9.catalogo_equipo_de is not null then t9.catalogo_equipo_de else "Descripcion no disponible" end catalogo_equipo_de,
case when t9.estado_suscripcion_id is not null then t9.estado_suscripcion_id else "-3" end estado_suscripcion_id,
case when t9.estado_suscripcion_cd is not null then t9.estado_suscripcion_cd else "-3" end estado_suscripcion_cd,
case when t9.estado_suscripcion_de is not null then t9.estado_suscripcion_de else "Descripcion no disponible" end estado_suscripcion_de,
case when t9.fecha_estado_suscripcion_id is not null then t9.fecha_estado_suscripcion_id else date("1900-01-01") end fecha_estado_suscripcion_id,
case when t9.cliente_id is not null then t9.cliente_id else "-3" end cliente_id,
case when t9.cliente_cd is not null then t9.cliente_cd else "-3" end cliente_cd,
case when t9.sub_tipo_cliente_id is not null then t9.sub_tipo_cliente_id else "-3" end sub_tipo_cliente_id,
case when t9.sub_tipo_cliente_cd is not null then t9.sub_tipo_cliente_cd else "-3" end sub_tipo_cliente_cd,
case when t9.sub_tipo_cliente_de is not null then t9.sub_tipo_cliente_de else "Descripcion no disponible" end sub_tipo_cliente_de,
case when t9.tipo_cliente_id is not null then t9.tipo_cliente_id else "-3" end tipo_cliente_id,
case when t9.tipo_cliente_cd is not null then t9.tipo_cliente_cd else "-3" end tipo_cliente_cd,
case when t9.tipo_cliente_de is not null then t9.tipo_cliente_de else "Descripcion no disponible" end tipo_cliente_de,
case when t9.ciclo_facturacion_id is not null then t9.ciclo_facturacion_id else "-3" end ciclo_facturacion_id,
case when t9.ciclo_facturacion_cd is not null then t9.ciclo_facturacion_cd else "-3" end ciclo_facturacion_cd,
case when t9.ciclo_facturacion_de is not null then t9.ciclo_facturacion_de else "Descripcion no disponible" end ciclo_facturacion_de,
case when t9.score_economico_id is not null then t9.score_economico_id else "-3" end score_economico_id,
case when t9.score_economico_cd is not null then t9.score_economico_cd else "-3" end score_economico_cd,
case when t9.score_economico_de is not null then t9.score_economico_de else "Descripcion no disponible" end score_economico_de,
case when t9.operadora_larga_distancia_id is not null then t9.operadora_larga_distancia_id else "-3" end operadora_larga_distancia_id,
case when t9.operadora_larga_distancia_cd is not null then t9.operadora_larga_distancia_cd else "-3" end operadora_larga_distancia_cd,
case when t9.operadora_larga_distancia_de is not null then t9.operadora_larga_distancia_de else "Descripcion no disponible" end operadora_larga_distancia_de,
case when t0.sistema_origen_id = "-3" then true else false end or 
case when t0.actuacion_subtipo_id = "-3" then true else false end or 
case when t0.metodo_recarga_id = "-3" then true else false end or 
case when t0.motivo_ajuste_id  = "-3" then true else false end or 
case when t0.moneda_id = "-3" then true else false end or 
case when t0.bono_id = "-3" then true else false end or 
case when t0.promocion_id = "-3" then true else false end or 
case when t9.suscripcion_id is null then true else false end or 
case when t9.tipo_oferta_id is null then true else false end or 
case when t9.catalogo_producto_id is null then true else false end or 
case when t9.geografia_cnc_id is null then true else false end or 
case when t9.sub_region_cnc_id is null then true else false end or 
case when t9.region_cnc_id is null then true else false end or 
case when t9.flag_prepago_express_id is null then true else false end or 
case when t9.estado_suscripcion_id is null then true else false end or 
case when t9.cliente_id is null then true else false end or 
case when t9.sub_tipo_cliente_id is null then true else false end or 
case when t9.tipo_cliente_id is null then true else false end or 
case when t9.ciclo_facturacion_id is null then true else false end or 
case when t9.score_economico_id is null then true else false end reinyectable,
concat(
case when t0.sistema_origen_id = "-3" then "00034," else "" end,
case when t0.actuacion_subtipo_id = "-3" then "00038," else "" end,
case when t0.metodo_recarga_id = "-3" then "00039," else "" end,
case when t0.motivo_ajuste_id  = "-3" then "00040," else "" end,
case when t0.moneda_id = "-3" then "00030," else "" end,
case when t0.bono_id = "-3" then "00041," else "" end,
case when t0.promocion_id = "-3" then "00042," else "" end,
case when t9.suscripcion_id is null then "00001," else "" end,
case when t9.tipo_oferta_id is null then "00002," else "" end,
case when t9.catalogo_producto_id is null then "00003," else "" end,
case when t9.geografia_cnc_id is null then "00004," else "" end,
case when t9.sub_region_cnc_id is null then "00005," else "" end,
case when t9.region_cnc_id is null then "00006," else "" end,
case when t9.flag_prepago_express_id is null then "00007," else "" end,
case when t9.estado_suscripcion_id is null then "00015," else "" end,
case when t9.cliente_id is null then "00016," else "" end,
case when t9.sub_tipo_cliente_id is null then "00017," else "" end,
case when t9.tipo_cliente_id is null then "00018," else "" end,
case when t9.ciclo_facturacion_id is null then "00019," else "" end,
case when t9.score_economico_id is null then "00020" else "" end) error_cd
from
${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp7 t0
left join ${DBNAME_EDW}.suscriptorizacion t9 on t0.ani_cd = t9.ani_cd
where
t0.operacion_fh >= t9.inicio_fh and 
t0.operacion_fh <= t9.fin_fh;

insert into ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_final 
select
t0.*,
"-3" suscripcion_id,
"-3" suscripcion_cd,
"-3" interurbano_cd,
"-3" urbano_cd,
"-3" linea_cd,
"-3" tipo_oferta_id,
"Descripcion no disponible" tipo_oferta_de,
"-3" catalogo_producto_id,
"-3" catalogo_producto_cd,
"Descripcion no disponible" catalogo_producto_de,
"-3" geografia_cnc_id,
"-3" geografia_cnc_cd,
"-3" sub_alm_cd,
"-3" alm_cd,
"-3" sub_region_cnc_id,
"Descripcion no disponible" sub_region_cnc_de,
"-3" region_cnc_id,
"Descripcion no disponible" region_cnc_de,
"-3" flag_prepago_express_id,
"-3" operadora_propietaria_id,
"-3" operadora_propietaria_cd,
"Descripcion no disponible" operadora_propietaria_de,
"-3" operadora_receptora_id,
"-3" operadora_receptora_cd,
"Descripcion no disponible" operadora_receptora_de,
"-3" operadora_donante_id,
"-3" operadora_donante_cd,
"Descripcion no disponible" operadora_donante_de,
"-3" flag_volte_id,
"-3" flag_volte_cd,
"Descripcion no disponible" flag_volte_de,
"-3" equipo_tecnologia_id,
"-3" equipo_tecnologia_cd,
"Descripcion no disponible" equipo_tecnologia_de,
"-3" equipo_utilizado_tecnologia_id,
"-3" equipo_utilizado_tecnologia_cd,
"Descripcion no disponible" equipo_utilizado_tecnologia_de,
"-3" catalogo_equipo_id,
"-3" catalogo_equipo_cd,
"Descripcion no disponible" catalogo_equipo_de,
"-3" estado_suscripcion_id,
"-3" estado_suscripcion_cd,
"Descripcion no disponible" estado_suscripcion_de,
date("1900-01-01") fecha_estado_suscripcion_id,
"-3" cliente_id,
"-3" cliente_cd,
"-3" sub_tipo_cliente_id,
"-3" sub_tipo_cliente_cd,
"Descripcion no disponible" sub_tipo_cliente_de,
"-3" tipo_cliente_id,
"-3" tipo_cliente_cd,
"Descripcion no disponible" tipo_cliente_de,
"-3" ciclo_facturacion_id,
"-3" ciclo_facturacion_cd,
"Descripcion no disponible" ciclo_facturacion_de,
"-3" score_economico_id,
"-3" score_economico_cd,
"Descripcion no disponible" score_economico_de,
"-3" operadora_larga_distancia_id,
"-3" operadora_larga_distancia_cd,
"Descripcion no disponible" operadora_larga_distancia_de,
true reinyectable,
concat(
case when t0.sistema_origen_id = "-3" then "00034," else "" end,
case when t0.actuacion_subtipo_id = "-3" then "00038," else "" end,
case when t0.metodo_recarga_id = "-3" then "00039," else "" end,
case when t0.motivo_ajuste_id  = "-3" then "00040," else "" end,
case when t0.moneda_id = "-3" then "00030," else "" end,
case when t0.bono_id = "-3" then "00041," else "" end,
case when t0.promocion_id = "-3" then "00042," else "" end,
"00001,",
"00002,",
"00003,",
"00004,",
"00005,",
"00006,",
"00007,",
"00015,",
"00016,",
"00017,",
"00018,",
"00019,",
"00020") error_cd
from ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_temp7 t0
where t0.hash_block_7 not in (select distinct hash_block_7 from ${DBNAME_EDW}.f_tr_actuacion_detallada_prev_final);

