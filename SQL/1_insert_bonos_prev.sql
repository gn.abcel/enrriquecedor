truncate table ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_prev;

insert into ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_prev
select
ani_cd,
tipo_enlace_de,
enlace_de     ,
valor_contador_ca,
fin_promocion_fh,
sesiones_usuario_ca,
identificador_promocion_nr  ,
identificador_tipo_bolton_nr as identificador_tipo_bono_nr,
costo_renovacion_subperiodo_mo,
ultima_renovacion_fh,
fin_subperiodo_renovado_fh,
unidades_al_renovar_ca,
vencimiento_subperiodo_fh,
nombre_extractor_tx,
fecha as fecha_fh
from trafico.altamira_bonos_movil_fp
where fecha = ${CURRENT_DATE_N}

union all

select 
ani_cd,
tipo_enlace_de,
enlace_de,
valor_contador_ca,
fin_promocion_fh,
sesiones_usuario_ca,
identificador_promocion_nr  ,
identificador_tipo_bolton_nr as identificador_tipo_bono_nr,
costo_renovacion_subperiodo_mo    ,
ultima_renovacion_fh        ,
fin_subperiodo_renovado_fh  ,
unidades_al_renovar_ca      ,
vencimiento_subperiodo_fh   ,
nombre_extractor_tx,
fecha as fecha_fh
from trafico.altamira_bonos_fija_fp
where fecha = ${CURRENT_DATE_N}
union all

select 
ani_cd,
tipo_enlace_de,
enlace_de     ,
valor_contador_ca  ,
fin_promocion_fh   ,
sesiones_usuario_ca,
identificador_promocion_nr  ,
identificador_tipo_bono_nr,
costo_renovacion_subperiodo_mo    ,
ultima_renovacion_fh        ,
fin_subperiodo_renovado_fh  ,
unidades_al_renovar_ca      ,
vencimiento_subperiodo_fh   ,
nombre_extractor_tx,
fecha_fh
from ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect;
