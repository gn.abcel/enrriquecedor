drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp1;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp1 as
select
t0.*,
case when t1.acumulador_cd is not null then t1.acumulador_cd else "-3" end acumulador_cd,
case when t2.acumulador_id is not null then t2.acumulador_id else "-3" end acumulador_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev t0
left join ${DBNAME_EDW}.d_eventprom_dim t1 on t0.ani_cd = t1.ani_cd and t0.inicio_sesion_datos_fh = t1.inicio_sesion_datos_fh and t0.codigo_identificador_cd = t1.codigo_identificador_cd
left join ${DBNAME_EDW}.d_acumulador t2 on t1.acumulador_cd = t2.acumulador_cd;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp2;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp2 as
select
t0.*,
case when t3.ambito_registro_id is not null then t3.ambito_registro_id else "-3" end ambito_registro_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp1 t0
left join ${DBNAME_EDW}.d_ambito_registro t3 on t3.ambito_registro_cd ='6';

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp3;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp3 as
select
t0.*,
case when t4.clase_descuento_aplicado_id is not null then t4.clase_descuento_aplicado_id else "-3" end clase_descuento_aplicado_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp2 t0
left join ${DBNAME_EDW}.d_clase_descuento_aplicado t4 on t0.plan_tarifario_cd = t4.clase_descuento_aplicado_cd;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp4;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp4 as
select
t0.*,
case when t5.clase_tarifa_id is not null then t5.clase_tarifa_id else "-3" end clase_tarifa_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp3 t0
left join ${DBNAME_EDW}.d_clase_tarifa t5 on cast(case when split(t0.nombre_archivo_de,'_')[5] = 'POS' then -2 else t0.clase_tarifa_cd end as string ) = t5.clase_tarifa_cd;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp5;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp5 as
select
t0.*,
case when t6.detalle_servicio_trafico_id is not null then t6.detalle_servicio_trafico_id else "-3" end detalle_servicio_trafico_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp4 t0
left join ${DBNAME_EDW}.d_detalle_servicio_trafico t6 on t0.tipo_servicio_datos_cd  = t6.detalle_servicio_trafico_cd;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp7;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp7 as
select
t0.*,
case when t8.estado_valorizacion_id is not null then t8.estado_valorizacion_id else "-3" end estado_valorizacion_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp5 t0
left join ${DBNAME_EDW}.d_estado_valorizacion t8 on t8.estado_valorizacion_cd = case when t0.importe_costo_recarga_mo > 0 then 'DAT_SA' else case when t0.importe_costo_recarga_mo is null and t0.acumulador_id not in (-1, -2, -3) then 'DATB_SA' else '-1' end end;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp8;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp8 as
select
t0.*,
case when t9.ambito_red_id is not null then t9.ambito_red_id else "-3" end ambito_red_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp7 t0
left join ${DBNAME_EDW}.d_ambito_red t9 on t9.ambito_red_cd ='25';

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp9;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp9 as
select
t0.*,
case when t10.franja_horaria_id is not null then t10.franja_horaria_id else "-3" end franja_horaria_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp8 t0
left join ${DBNAME_EDW}.d_franja_horaria t10 on t10.franja_horaria_cd ='BU';

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp10;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp10 as
select
t0.*,
case when t11.sentido_trafico_id is not null then t11.sentido_trafico_id else "-3" end sentido_trafico_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp9 t0
left join ${DBNAME_EDW}.d_sentido_trafico t11 on t11.sentido_trafico_cd ='S';

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp11;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp11 as
select
t0.*,
case when t12.moneda_id is not null then t12.moneda_id else "-3" end moneda_id,
case when t13.rango_duracion_llamada_id is not null then t13.rango_duracion_llamada_id else "-3" end rango_duracion_llamada_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp10 t0
left join ${DBNAME_EDW}.d_moneda t12 on t12.moneda_cd ='ARS'
left join ${DBNAME_EDW}.d_rango_duracion_llamada_detail t13 on cast(t0.duracion_sesion_datos_mo/60 as int) = t13.rango_duracion;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp12;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp12 as
select
t0.*,
case when t14.sistema_origen_id is not null then t14.sistema_origen_id else "-3" end sistema_origen_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp11 t0
left join ${DBNAME_EDW}.d_sistema_origen t14 on t14.sistema_origen_cd ='ALT';

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp13;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp13 as
select
t0.*,
case when t15.plataforma_id is not null then t15.plataforma_id else "-3" end plataforma_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp12 t0
left join ${DBNAME_EDW}.d_plataforma t15 on t15.plataforma_cd ='-1';

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp14;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp14 as
select
t0.*,
case when t16.tramo_tarifario_id is not null then t16.tramo_tarifario_id else "-3" end tramo_tarifario_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp13 t0
left join ${DBNAME_EDW}.d_tramo_tarifario t16 on case when t0.importe_cobrado_mo > 0 then 1 else 0 end = t16.tramo_tarifario_cd;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp15;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp15 as
select
t0.*,
date_format(t0.inicio_sesion_datos_fh,'YYYY-MM-dd') fecha_inicio_sesion_id,
case when t17.hora_id is not null then t17.hora_id else "-3" end hora_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp14 t0
left join ${DBNAME_EDW}.d_hora t17 on date_format(t0.inicio_sesion_datos_fh, 'HH:00') = t17.hora_24_de;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp16;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp16 as
select
t0.*,
md5(concat(t0.ani_cd, t0.inicio_sesion_datos_fh)) hash_block_16,
case when (case when t0.tecnologia_acceso_red_cd is null then '-2' else case when t19.tecnologia_acceso_cd is null then -3 else  t19.tecnologia_acceso_id end end ) is not null then (case when t0.tecnologia_acceso_red_cd is null then '-2' else case when t19.tecnologia_acceso_cd is null then -3 else  t19.tecnologia_acceso_id end end ) else "-3" end tecnologia_acceso_id
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp15 t0
left join ${DBNAME_EDW}.d_tecnologia_acceso t19 on t0.tecnologia_acceso_red_cd = t19.tecnologia_acceso_cd;

drop table ${DBNAME_EDW}.f_tr_datos_detallada_prev_final;
create table ${DBNAME_EDW}.f_tr_datos_detallada_prev_final as
select
t0.*,
case when t21.suscripcion_id is not null then t21.suscripcion_id else "-3" end suscripcion_id,
case when t21.suscripcion_cd is not null then t21.suscripcion_cd else "-3" end suscripcion_cd,
case when t21.interurbano_cd is not null then t21.interurbano_cd else "-3" end interurbano_cd,
case when t21.urbano_cd is not null then t21.urbano_cd else "-3" end urbano_cd,
case when t21.linea_cd is not null then t21.linea_cd else "-3" end linea_cd,
case when t21.tipo_oferta_id is not null then t21.tipo_oferta_id else "-3" end tipo_oferta_id,
case when t21.tipo_oferta_de is not null then t21.tipo_oferta_de else "Descripcion no disponible" end tipo_oferta_de,
case when t21.catalogo_producto_id is not null then t21.catalogo_producto_id else "-3" end catalogo_producto_id,
case when t21.catalogo_producto_cd is not null then t21.catalogo_producto_cd else "-3" end catalogo_producto_cd,
case when t21.catalogo_producto_de is not null then t21.catalogo_producto_de else "Descripcion no disponible" end catalogo_producto_de,
case when t21.geografia_cnc_id is not null then t21.geografia_cnc_id else "-3" end geografia_cnc_id,
case when t21.geografia_cnc_cd is not null then t21.geografia_cnc_cd else "-3" end geografia_cnc_cd,
case when t21.sub_alm_cd is not null then t21.sub_alm_cd else "-3" end sub_alm_cd,
case when t21.alm_cd is not null then t21.alm_cd else "-3" end alm_cd,
case when t21.sub_region_cnc_id is not null then t21.sub_region_cnc_id else "-3" end sub_region_cnc_id,
case when t21.sub_region_cnc_de is not null then t21.sub_region_cnc_de else "Descripcion no disponible" end sub_region_cnc_de,
case when t21.region_cnc_id is not null then t21.region_cnc_id else "-3" end region_cnc_id,
case when t21.region_cnc_de is not null then t21.region_cnc_de else "Descripcion no disponible" end region_cnc_de,
case when t21.flag_prepago_express_id is not null then t21.flag_prepago_express_id else "-3" end flag_prepago_express_id,
case when t21.operadora_propietaria_id is not null then t21.operadora_propietaria_id else "-3" end operadora_propietaria_id,
case when t21.operadora_propietaria_cd is not null then t21.operadora_propietaria_cd else "-3" end operadora_propietaria_cd,
case when t21.operadora_propietaria_de is not null then t21.operadora_propietaria_de else "Descripcion no disponible" end operadora_propietaria_de,
case when t21.operadora_receptora_id is not null then t21.operadora_receptora_id else "-3" end operadora_receptora_id,
case when t21.operadora_receptora_cd is not null then t21.operadora_receptora_cd else "-3" end operadora_receptora_cd,
case when t21.operadora_receptora_de is not null then t21.operadora_receptora_de else "Descripcion no disponible" end operadora_receptora_de,
case when t21.operadora_donante_id is not null then t21.operadora_donante_id else "-3" end operadora_donante_id,
case when t21.operadora_donante_cd is not null then t21.operadora_donante_cd else "-3" end operadora_donante_cd,
case when t21.operadora_donante_de is not null then t21.operadora_donante_de else "Descripcion no disponible" end operadora_donante_de,
case when t21.flag_volte_id is not null then t21.flag_volte_id else "-3" end flag_volte_id,
case when t21.flag_volte_cd is not null then t21.flag_volte_cd else "-3" end flag_volte_cd,
case when t21.flag_volte_de is not null then t21.flag_volte_de else "Descripcion no disponible" end flag_volte_de,
case when t21.equipo_tecnologia_id is not null then t21.equipo_tecnologia_id else "-3" end equipo_tecnologia_id,
case when t21.equipo_tecnologia_cd is not null then t21.equipo_tecnologia_cd else "-3" end equipo_tecnologia_cd,
case when t21.equipo_tecnologia_de is not null then t21.equipo_tecnologia_de else "Descripcion no disponible" end equipo_tecnologia_de,
case when t21.equipo_utilizado_tecnologia_id is not null then t21.equipo_utilizado_tecnologia_id else "-3" end equipo_utilizado_tecnologia_id,
case when t21.equipo_utilizado_tecnologia_cd is not null then t21.equipo_utilizado_tecnologia_cd else "-3" end equipo_utilizado_tecnologia_cd,
case when t21.equipo_utilizado_tecnologia_de is not null then t21.equipo_utilizado_tecnologia_de else "Descripcion no disponible" end equipo_utilizado_tecnologia_de,
case when t21.catalogo_equipo_id is not null then t21.catalogo_equipo_id else "-3" end catalogo_equipo_id,
case when t21.catalogo_equipo_cd is not null then t21.catalogo_equipo_cd else "-3" end catalogo_equipo_cd,
case when t21.catalogo_equipo_de is not null then t21.catalogo_equipo_de else "Descripcion no disponible" end catalogo_equipo_de,
case when t21.estado_suscripcion_id is not null then t21.estado_suscripcion_id else "-3" end estado_suscripcion_id,
case when t21.estado_suscripcion_cd is not null then t21.estado_suscripcion_cd else "-3" end estado_suscripcion_cd,
case when t21.estado_suscripcion_de is not null then t21.estado_suscripcion_de else "Descripcion no disponible" end estado_suscripcion_de,
case when t21.fecha_estado_suscripcion_id is not null then t21.fecha_estado_suscripcion_id else date("1900-01-01") end fecha_estado_suscripcion_id,
case when t21.cliente_id is not null then t21.cliente_id else "-3" end cliente_id,
case when t21.cliente_cd is not null then t21.cliente_cd else "-3" end cliente_cd,
case when t21.sub_tipo_cliente_id is not null then t21.sub_tipo_cliente_id else "-3" end sub_tipo_cliente_id,
case when t21.sub_tipo_cliente_cd is not null then t21.sub_tipo_cliente_cd else "-3" end sub_tipo_cliente_cd,
case when t21.sub_tipo_cliente_de is not null then t21.sub_tipo_cliente_de else "Descripcion no disponible" end sub_tipo_cliente_de,
case when t21.tipo_cliente_id is not null then t21.tipo_cliente_id else "-3" end tipo_cliente_id,
case when t21.tipo_cliente_cd is not null then t21.tipo_cliente_cd else "-3" end tipo_cliente_cd,
case when t21.tipo_cliente_de is not null then t21.tipo_cliente_de else "Descripcion no disponible" end tipo_cliente_de,
case when t21.ciclo_facturacion_id is not null then t21.ciclo_facturacion_id else "-3" end ciclo_facturacion_id,
case when t21.ciclo_facturacion_cd is not null then t21.ciclo_facturacion_cd else "-3" end ciclo_facturacion_cd,
case when t21.ciclo_facturacion_de is not null then t21.ciclo_facturacion_de else "Descripcion no disponible" end ciclo_facturacion_de,
case when t21.score_economico_id is not null then t21.score_economico_id else "-3" end score_economico_id,
case when t21.score_economico_cd is not null then t21.score_economico_cd else "-3" end score_economico_cd,
case when t21.score_economico_de is not null then t21.score_economico_de else "Descripcion no disponible" end score_economico_de,
case when t21.operadora_larga_distancia_id is not null then t21.operadora_larga_distancia_id else "-3" end operadora_larga_distancia_id,
case when t21.operadora_larga_distancia_cd is not null then t21.operadora_larga_distancia_cd else "-3" end operadora_larga_distancia_cd,
case when t21.operadora_larga_distancia_de is not null then t21.operadora_larga_distancia_de else "Descripcion no disponible" end operadora_larga_distancia_de,
case when t0.acumulador_id = "-3" then true else false end or 
case when t0.ambito_registro_id = "-3" then true else false end or 
case when t0.clase_descuento_aplicado_id = "-3" then true else false end or 
case when t0.clase_tarifa_id = "-3" then true else false end or 
case when t0.detalle_servicio_trafico_id = "-3" then true else false end or 
case when t0.estado_valorizacion_id = "-3" then true else false end or 
case when t0.ambito_red_id = "-3" then true else false end or 
case when t0.franja_horaria_id = "-3" then true else false end or 
case when t0.sentido_trafico_id = "-3" then true else false end or 
case when t0.moneda_id = "-3" then true else false end or 
case when t0.rango_duracion_llamada_id = "-3" then true else false end or 
case when t0.sistema_origen_id = "-3" then true else false end or 
case when t0.plataforma_id = "-3" then true else false end or 
case when t0.tramo_tarifario_id = "-3" then true else false end or 
case when t0.tecnologia_acceso_id = "-3" then true else false end or 
case when t21.suscripcion_id is null then true else false end or 
case when t21.tipo_oferta_id is null then true else false end or 
case when t21.catalogo_producto_id is null then true else false end or 
case when t21.geografia_cnc_id is null then true else false end or 
case when t21.sub_region_cnc_id is null then true else false end or 
case when t21.region_cnc_id is null then true else false end or 
case when t21.flag_prepago_express_id is null then true else false end or 
case when t21.estado_suscripcion_id is null then true else false end or 
case when t21.cliente_id is null then true else false end or 
case when t21.sub_tipo_cliente_id is null then true else false end or 
case when t21.tipo_cliente_id is null then true else false end or 
case when t21.ciclo_facturacion_id is null then true else false end or 
case when t21.score_economico_id is null then true else false end reinyectable,
concat(
case when t0.acumulador_id = "-3" then "00032," else "" end,
case when t0.ambito_registro_id = "-3" then "00022," else "" end,
case when t0.clase_descuento_aplicado_id = "-3" then "00023," else "" end,
case when t0.clase_tarifa_id = "-3" then "00024," else "" end,
case when t0.detalle_servicio_trafico_id = "-3" then "00025," else "" end,
case when t0.estado_valorizacion_id = "-3" then "00026," else "" end,
case when t0.ambito_red_id = "-3" then "00027," else "" end,
case when t0.franja_horaria_id = "-3" then "00028," else "" end,
case when t0.sentido_trafico_id = "-3" then "00029," else "" end,
case when t0.moneda_id = "-3" then "00030," else "" end,
case when t0.rango_duracion_llamada_id = "-3" then "00033," else "" end,
case when t0.sistema_origen_id = "-3" then "00034," else "" end,
case when t0.plataforma_id = "-3" then "00031," else "" end,
case when t0.tramo_tarifario_id = "-3" then "00036," else "" end,
case when t0.tecnologia_acceso_id = "-3" then "00035," else "" end,
case when t21.suscripcion_id is null then "00001," else "" end,
case when t21.tipo_oferta_id is null then "00002," else "" end,
case when t21.catalogo_producto_id is null then "00003," else "" end,
case when t21.geografia_cnc_id is null then "00004," else "" end,
case when t21.sub_region_cnc_id is null then "00005," else "" end,
case when t21.region_cnc_id is null then "00006," else "" end,
case when t21.flag_prepago_express_id is null then "00007," else "" end,
case when t21.estado_suscripcion_id is null then "00015," else "" end,
case when t21.cliente_id is null then "00016," else "" end,
case when t21.sub_tipo_cliente_id is null then "00017," else "" end,
case when t21.tipo_cliente_id is null then "00018," else "" end,
case when t21.ciclo_facturacion_id is null then "00019," else "" end,
case when t21.score_economico_id is null then "00020" else "" end) error_cd
from
${DBNAME_EDW}.f_tr_datos_detallada_prev_temp16 t0
left join ${DBNAME_EDW}.suscriptorizacion t21 on t0.ani_cd = t21.ani_cd
where
t0.inicio_sesion_datos_fh >= t21.inicio_fh and 
t0.inicio_sesion_datos_fh <= t21.fin_fh	;

insert into ${DBNAME_EDW}.f_tr_datos_detallada_prev_final 
select
t0.*,
"-3" suscripcion_id,
"-3" suscripcion_cd,
"-3" interurbano_cd,
"-3" urbano_cd,
"-3" linea_cd,
"-3" tipo_oferta_id,
"Descripcion no disponible" tipo_oferta_de,
"-3" catalogo_producto_id,
"-3" catalogo_producto_cd,
"Descripcion no disponible" catalogo_producto_de,
"-3" geografia_cnc_id,
"-3" geografia_cnc_cd,
"-3" sub_alm_cd,
"-3" alm_cd,
"-3" sub_region_cnc_id,
"Descripcion no disponible" sub_region_cnc_de,
"-3" region_cnc_id,
"Descripcion no disponible" region_cnc_de,
"-3" flag_prepago_express_id,
"-3" operadora_propietaria_id,
"-3" operadora_propietaria_cd,
"Descripcion no disponible" operadora_propietaria_de,
"-3" operadora_receptora_id,
"-3" operadora_receptora_cd,
"Descripcion no disponible" operadora_receptora_de,
"-3" operadora_donante_id,
"-3" operadora_donante_cd,
"Descripcion no disponible" operadora_donante_de,
"-3" flag_volte_id,
"-3" flag_volte_cd,
"Descripcion no disponible" flag_volte_de,
"-3" equipo_tecnologia_id,
"-3" equipo_tecnologia_cd,
"Descripcion no disponible" equipo_tecnologia_de,
"-3" equipo_utilizado_tecnologia_id,
"-3" equipo_utilizado_tecnologia_cd,
"Descripcion no disponible" equipo_utilizado_tecnologia_de,
"-3" catalogo_equipo_id,
"-3" catalogo_equipo_cd,
"Descripcion no disponible" catalogo_equipo_de,
"-3" estado_suscripcion_id,
"-3" estado_suscripcion_cd,
"Descripcion no disponible" estado_suscripcion_de,
date("1900-01-01") fecha_estado_suscripcion_id,
"-3" cliente_id,
"-3" cliente_cd,
"-3" sub_tipo_cliente_id,
"-3" sub_tipo_cliente_cd,
"Descripcion no disponible" sub_tipo_cliente_de,
"-3" tipo_cliente_id,
"-3" tipo_cliente_cd,
"Descripcion no disponible" tipo_cliente_de,
"-3" ciclo_facturacion_id,
"-3" ciclo_facturacion_cd,
"Descripcion no disponible" ciclo_facturacion_de,
"-3" score_economico_id,
"-3" score_economico_cd,
"Descripcion no disponible" score_economico_de,
"-3" operadora_larga_distancia_id,
"-3" operadora_larga_distancia_cd,
"Descripcion no disponible" operadora_larga_distancia_de,
true reinyectable,
concat(
case when t0.acumulador_id = "-3" then "00032," else "" end,
case when t0.ambito_registro_id = "-3" then "00022," else "" end,
case when t0.clase_descuento_aplicado_id = "-3" then "00023," else "" end,
case when t0.clase_tarifa_id = "-3" then "00024," else "" end,
case when t0.detalle_servicio_trafico_id = "-3" then "00025," else "" end,
case when t0.estado_valorizacion_id = "-3" then "00026," else "" end,
case when t0.ambito_red_id = "-3" then "00027," else "" end,
case when t0.franja_horaria_id = "-3" then "00028," else "" end,
case when t0.sentido_trafico_id = "-3" then "00029," else "" end,
case when t0.moneda_id = "-3" then "00030," else "" end,
case when t0.rango_duracion_llamada_id = "-3" then "00033," else "" end,
case when t0.sistema_origen_id = "-3" then "00034," else "" end,
case when t0.plataforma_id = "-3" then "00031," else "" end,
case when t0.tramo_tarifario_id = "-3" then "00036," else "" end,
case when t0.tecnologia_acceso_id = "-3" then "00035," else "" end,
"00001,",
"00002,",
"00003,",
"00004,",
"00005,",
"00006,",
"00007,",
"00015,",
"00016,",
"00017,",
"00018,",
"00019,",
"00020") error_cd
from ${DBNAME_EDW}.f_tr_datos_detallada_prev_temp16 t0
where t0.hash_block_16 not in (select distinct hash_block_16 from ${DBNAME_EDW}.f_tr_datos_detallada_prev_final);

