
-- Insert en la particion por Fecha de Evento, sin criterio de reinyectable en el WHERE Clause (Se mezclarian con los suscriptorizados)
insert into table ${DBNAME_EDW}.f_ps_bonos_sdp_detallada partition(fecha)
select 
ani_cd,
tipo_enlace_de,
enlace_de,
valor_contador_ca,
fin_promocion_fh,
sesiones_usuario_ca,
identificador_promocion_nr,
identificador_tipo_bono_nr,
costo_renovacion_subperiodo_mo,
ultima_renovacion_fh,
fin_subperiodo_renovado_fh,
unidades_al_renovar_ca,
vencimiento_subperiodo_fh,
nombre_extractor_tx,
fecha_fh,

sistema_origen_id,
acumulador_id,
tipo_acumulador_id,

suscripcion_id ,
suscripcion_cd ,
interurbano_cd ,
urbano_cd ,
linea_cd ,
tipo_oferta_id ,
tipo_oferta_de ,
catalogo_producto_id ,
catalogo_producto_cd ,
catalogo_producto_de ,
geografia_cnc_id ,
geografia_cnc_cd ,
sub_alm_cd ,
alm_cd ,
sub_region_cnc_id ,
sub_region_cnc_de ,
region_cnc_id ,
region_cnc_de ,
flag_prepago_express_id ,
operadora_propietaria_id ,
operadora_propietaria_cd ,
operadora_propietaria_de ,
operadora_receptora_id ,
operadora_receptora_cd ,
operadora_receptora_de ,
operadora_donante_id ,
operadora_donante_cd ,
operadora_donante_de ,
flag_volte_id ,
flag_volte_cd ,
flag_volte_de ,
equipo_tecnologia_id ,
equipo_tecnologia_cd ,
equipo_tecnologia_de ,
equipo_utilizado_tecnologia_id ,
equipo_utilizado_tecnologia_cd ,
equipo_utilizado_tecnologia_de ,
catalogo_equipo_id ,
catalogo_equipo_cd ,
catalogo_equipo_de ,
estado_suscripcion_id ,
estado_suscripcion_cd ,
estado_suscripcion_de ,
fecha_estado_suscripcion_id ,
cliente_id ,
cliente_cd ,
sub_tipo_cliente_id ,
sub_tipo_cliente_cd ,
sub_tipo_cliente_de ,
tipo_cliente_id ,
tipo_cliente_cd ,
tipo_cliente_de ,
ciclo_facturacion_id ,
ciclo_facturacion_cd ,
ciclo_facturacion_de ,
score_economico_id ,
score_economico_cd ,
score_economico_de ,
operadora_larga_distancia_id ,
operadora_larga_distancia_cd ,
operadora_larga_distancia_de ,
 cast(substr(split(nombre_extractor_tx,'_')[4],1,8) as int) as fecha
from ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect

where 
 cast(substr(split(nombre_extractor_tx,'_')[4],1,8) as int) <
cast(date_format(current_date,'yyyyMM01') as int );

DROP TABLE ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect_temp;

CREATE TABLE ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect_temp AS 
SELECT 
ani_cd,
tipo_enlace_de,
enlace_de,
valor_contador_ca,
fin_promocion_fh,
sesiones_usuario_ca,
identificador_promocion_nr,
identificador_tipo_bono_nr,
costo_renovacion_subperiodo_mo,
ultima_renovacion_fh,
fin_subperiodo_renovado_fh,
unidades_al_renovar_ca,
vencimiento_subperiodo_fh,
nombre_extractor_tx,
fecha_fh,

sistema_origen_id,
acumulador_id,
tipo_acumulador_id,

suscripcion_id ,
suscripcion_cd ,
interurbano_cd ,
urbano_cd ,
linea_cd ,
tipo_oferta_id ,
tipo_oferta_de ,
catalogo_producto_id ,
catalogo_producto_cd ,
catalogo_producto_de ,
geografia_cnc_id ,
geografia_cnc_cd ,
sub_alm_cd ,
alm_cd ,
sub_region_cnc_id ,
sub_region_cnc_de ,
region_cnc_id ,
region_cnc_de ,
flag_prepago_express_id ,
operadora_propietaria_id ,
operadora_propietaria_cd ,
operadora_propietaria_de ,
operadora_receptora_id ,
operadora_receptora_cd ,
operadora_receptora_de ,
operadora_donante_id ,
operadora_donante_cd ,
operadora_donante_de ,
flag_volte_id ,
flag_volte_cd ,
flag_volte_de ,
equipo_tecnologia_id ,
equipo_tecnologia_cd ,
equipo_tecnologia_de ,
equipo_utilizado_tecnologia_id ,
equipo_utilizado_tecnologia_cd ,
equipo_utilizado_tecnologia_de ,
catalogo_equipo_id ,
catalogo_equipo_cd ,
catalogo_equipo_de ,
estado_suscripcion_id ,
estado_suscripcion_cd ,
estado_suscripcion_de ,
fecha_estado_suscripcion_id ,
cliente_id ,
cliente_cd ,
sub_tipo_cliente_id ,
sub_tipo_cliente_cd ,
sub_tipo_cliente_de ,
tipo_cliente_id ,
tipo_cliente_cd ,
tipo_cliente_de ,
ciclo_facturacion_id ,
ciclo_facturacion_cd ,
ciclo_facturacion_de ,
score_economico_id ,
score_economico_cd ,
score_economico_de ,
operadora_larga_distancia_id ,
operadora_larga_distancia_cd ,
operadora_larga_distancia_de ,
error_cd ,
reinyectable 
from ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect
where 
 cast(substr(split(nombre_extractor_tx,'_')[4],1,8) as int) >=
cast(date_format(current_date,'yyyyMM01') as int );

insert overwrite table ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect
select
ani_cd,
tipo_enlace_de,
enlace_de,
valor_contador_ca,
fin_promocion_fh,
sesiones_usuario_ca,
identificador_promocion_nr,
identificador_tipo_bono_nr,
costo_renovacion_subperiodo_mo,
ultima_renovacion_fh,
fin_subperiodo_renovado_fh,
unidades_al_renovar_ca,
vencimiento_subperiodo_fh,
nombre_extractor_tx,
fecha_fh,

sistema_origen_id,
acumulador_id,
tipo_acumulador_id,

suscripcion_id ,
suscripcion_cd ,
interurbano_cd ,
urbano_cd ,
linea_cd ,
tipo_oferta_id ,
tipo_oferta_de ,
catalogo_producto_id ,
catalogo_producto_cd ,
catalogo_producto_de ,
geografia_cnc_id ,
geografia_cnc_cd ,
sub_alm_cd ,
alm_cd ,
sub_region_cnc_id ,
sub_region_cnc_de ,
region_cnc_id ,
region_cnc_de ,
flag_prepago_express_id ,
operadora_propietaria_id ,
operadora_propietaria_cd ,
operadora_propietaria_de ,
operadora_receptora_id ,
operadora_receptora_cd ,
operadora_receptora_de ,
operadora_donante_id ,
operadora_donante_cd ,
operadora_donante_de ,
flag_volte_id ,
flag_volte_cd ,
flag_volte_de ,
equipo_tecnologia_id ,
equipo_tecnologia_cd ,
equipo_tecnologia_de ,
equipo_utilizado_tecnologia_id ,
equipo_utilizado_tecnologia_cd ,
equipo_utilizado_tecnologia_de ,
catalogo_equipo_id ,
catalogo_equipo_cd ,
catalogo_equipo_de ,
estado_suscripcion_id ,
estado_suscripcion_cd ,
estado_suscripcion_de ,
fecha_estado_suscripcion_id ,
cliente_id ,
cliente_cd ,
sub_tipo_cliente_id ,
sub_tipo_cliente_cd ,
sub_tipo_cliente_de ,
tipo_cliente_id ,
tipo_cliente_cd ,
tipo_cliente_de ,
ciclo_facturacion_id ,
ciclo_facturacion_cd ,
ciclo_facturacion_de ,
score_economico_id ,
score_economico_cd ,
score_economico_de ,
operadora_larga_distancia_id ,
operadora_larga_distancia_cd ,
operadora_larga_distancia_de ,
error_cd ,
reinyectable 
from ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect_temp;

DROP TABLE ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect_temp;




