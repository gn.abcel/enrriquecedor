truncate table ${DBNAME_EDW}.f_ps_saldos_detallada_prev;

insert into ${DBNAME_EDW}.f_ps_saldos_detallada_prev
select
ani_cd,
saldo_cd ,
saldo_disponible_mo ,
caducidad_fh ,
saldo_bolsa_2_mo ,
caducidad_2_fh ,
saldo_bolsa_3_mo ,
caducidad_3_fh ,
saldo_bolsa_4_mo ,
caducidad_4_fh ,
saldo_bolsa_5_mo ,
caducidad_5_fh ,
saldo_bolsa_6_mo ,
caducidad_6_fh ,
deuda_pendiente_compensar_mo ,
inicio_deuda_fe,
habilitacion_uso_deuda_fl,
nombre_extractor_tx,
fecha as fecha_id

from trafico.altamira_saldos_movil_fp
where fecha = ${CURRENT_DATE_N}

union all

select 
ani_cd ,
saldo_cd,
saldo_disponible_mo ,
caducidad_fh,
saldo_bolsa_2_mo,
caducidad_2_fh,
saldo_bolsa_3_mo,
caducidad_3_fh,
saldo_bolsa_4_mo,
caducidad_4_fh,
saldo_bolsa_5_mo,
caducidad_5_fh,
saldo_bolsa_6_mo,
caducidad_6_fh,
deuda_pendiente_compensar_mo,
inicio_deuda_fe ,
habilitacion_uso_deuda_fl ,
nombre_extractor_tx,
fecha as fecha_id

from trafico.altamira_saldos_fija_fp
where fecha = ${CURRENT_DATE_N}

union all

select 
ani_cd,
saldo_cd,
saldo_disponible_mo,
caducidad_fh ,
saldo_bolsa_2_mo ,
caducidad_2_fh ,
saldo_bolsa_3_mo ,
caducidad_3_fh ,
saldo_bolsa_4_mo ,
caducidad_4_fh ,
saldo_bolsa_5_mo ,
caducidad_5_fh ,
saldo_bolsa_6_mo ,
caducidad_6_fh ,
deuda_pendiente_compensar_mo ,
inicio_deuda_fe,
habilitacion_uso_deuda_fl,
nombre_extractor_tx,
fecha_id
from ${DBNAME_EDW}.f_ps_saldos_detallada_reinyect;
