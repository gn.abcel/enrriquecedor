DROP TABLE ${DBNAME_EDW}.f_tr_datos_detallada_prev;

create table ${DBNAME_EDW}.f_tr_datos_detallada_prev
(
ani_cd	string	,
inicio_sesion_datos_fh	timestamp	,
plan_tarifario_cd	string	,
tipo_servicio_datos_cd	string	,
duracion_sesion_datos_mo	bigint	,
cantidad_enviada_bytes_mo	bigint	,
cantidad_recibida_bytes_mo	bigint	,
indicador_roaming_cd	string	,
importe_cobrado_mo	double	,
importe_costo_inicial_mo	double	,
importe_costo_abono_mo	double	,
importe_costo_recarga_mo	double	,
importe_costo_promocion_mo	double	,
importe_costo_bolsa5_mo	double	,
importe_costo_bolsa6_mo	double	,
importe_no_cargado_mo	double	,
impuesto_iva_mo	double	,
operadora_cd	bigint	,
tecnologia_acceso_red_cd	string	,
clase_tarifa_cd	bigint	,
identificador_archivo_cd	bigint	,
tipo_destino_cd	bigint	,
tipo_evento_cd	string	,
valor_eventprom_mo	double	,
imei_nr	string	,
identificador_sesion_cd	string	,
deuda_mo	double	,
identificador_celda_cd	string	,
codigo_identificador_cd	string	,
nombre_archivo_de	string	,
roaming_f	int	,
anio_mes_nr	int	,
flag_movil_fl	int	
)
stored as orc;
