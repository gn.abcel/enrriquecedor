drop table ${DBNAME_EDW}.f_ps_saldos_detallada_prev;

create table ${DBNAME_EDW}.f_ps_saldos_detallada_prev
(ani_cd	string,
saldo_cd	string,
saldo_disponible_mo	double,
caducidad_fh	timestamp,
saldo_bolsa_2_mo	double,
caducidad_2_fh	timestamp,
saldo_bolsa_3_mo	double,
caducidad_3_fh	timestamp,
saldo_bolsa_4_mo	double,
caducidad_4_fh	timestamp,
saldo_bolsa_5_mo	double,
caducidad_5_fh	timestamp,
saldo_bolsa_6_mo	double,
caducidad_6_fh	timestamp,
deuda_pendiente_compensar_mo	double,
inicio_deuda_fe	double,
habilitacion_uso_deuda_fl	string,
nombre_extractor_tx	string,
fecha_id	int
)
stored as orc;