drop table edw_desa_swf.f_ps_bonos_sdp_detallada_prev;

create table edw_desa_swf.f_ps_bonos_sdp_detallada_prev
(ani_cd string,
tipo_enlace_de string,
enlace_de string,
valor_contador_ca decimal(19,0),
fin_promocion_fh timestamp,
sesiones_usuario_ca int,
identificador_promocion_nr smallint,
identificador_tipo_bono_nr smallint,
costo_renovacion_subperiodo_mo decimal(19,0),
ultima_renovacion_fh timestamp,
fin_subperiodo_renovado_fh timestamp,
unidades_al_renovar_ca decimal(19,0),
vencimiento_subperiodo_fh timestamp,
nombre_extractor_tx string,
fecha_fh int)
stored as orc;


